const app = require('express')()
const db = require('./model').db
const cors = require('cors')

app.use(cors());

app.get('/api/locations', require('./router'))
module.exports = app