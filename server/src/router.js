const express = require("express");
const db = require('./model')
const router = express.Router();

router.get("/api/locations", (req, res, next) => {
    let sqlData = "SELECT * FROM LocationData";

    db.all(sqlData, (err, rows) => {
        try {
            res.json({
                results: rows
            });
        } catch (e) {
            res.status(400).json({ error: err.message });
            return;
        }
    });
});

module.exports = router