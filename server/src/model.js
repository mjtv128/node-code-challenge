const express = require("express");
const sqlite3 = require("sqlite3").verbose();
const fs = require("fs");
const d3 = require("d3");

const db = new sqlite3.Database("database.db");

//creates database LocationData that has 4 tables
db.serialize(function() {
    db.run(
        "CREATE TABLE IF NOT EXISTS LocationData (geonameid TEXT,name TEXT, latitude TEXT, longitude TEXT)"
    );

    fs.readFile("data/GB.tsv", "utf8", function(error, data) {
        data = d3.tsvParse(data);
        const jsonStringify = JSON.stringify(data);

        let stmt = db.prepare("INSERT INTO LocationData VALUES (?,?,?,?)");
        for (let i = 0; i < data.length; i++) {
            stmt.run(
                data[i].geonameid,
                data[i].name,
                data[i].latitude,
                data[i].longitude
            );
        }
        stmt.finalize();
    });
});



module.exports = db