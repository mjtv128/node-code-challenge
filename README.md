To run this:

 Please go into the server file ```cd server``` and run ```node server.js``` OR run ```cd server && node server.js```

 To run the client side: Please go into the client file ```cd client``` and run ```npm start``` OR alternatively ```cd client && npm start```

-----

This backend uses SQLite3 server to hold the TSV data that was provided in this code challenge. The TSV data contains information on different locations. 

To set up a SQLite database, the .tsv file was read using the npm fs. For this challenge, I am interested only in the geonameid, location name and their coordinates so they were imported to the SQLite3 database
. 
Once it has been imported into SQLite3, an API was created on http://localhost:3000/api/locations that contained the location's id, name, longitude and latitude. This API was created so that when a user searches for a location, the client will be able to send a get request to this APi to retrieve their desired information.

Users can search through an existing database by simply typing a name they wish to search for. Results will be shown when 3 or more characters are inputted. 

Results will display a list of locations matching the keyword search as well as their corresponding coordinates (latitude and longitude)

When a user types in their location name, the client will use that search term and send a get HTTP request to the server to retrieve the information.