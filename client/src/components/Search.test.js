import React from "react";
import { render } from "@testing-library/react";
import { shallow, mount } from "enzyme";
import Search from "./Search";
import Input from "./Input";
import SearchResults from "./SearchResults";

const mockData = [
    {
        geonameid: "123",
        name: "Location Name",
        latitude: "1234",
        longitude: "35"
    }
];

describe('tests Search', () => {
    it("checks the type", () => {
        const wrapper = shallow( < Search /> );

        expect(wrapper).toEqual({});
    });

    it('renders search results when the articles change', () => {
        const wrapper = mount(<Search articles={[]}/>)

        wrapper.setProps({
            result: mockData
        })

        expect(wrapper.find('li')).toEqual({})
    });

})