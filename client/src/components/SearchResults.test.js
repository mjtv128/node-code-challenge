import React from "react";
import { render } from "@testing-library/react";
import { shallow } from "enzyme";
import SearchResults from "./SearchResults";

const mockData = [{
    geonameid: "123",
    name: "Location Name",
    latitude: "1234",
    longitude: "35"
}];

describe("Search Results test", () => {
    it("doesnt break without results", () => {
        const wrapper = shallow(<SearchResults result={[]} />);

        expect(wrapper.find("li")).toHaveLength(0);
    });

    it("renders", () => {
        const wrapper = shallow(<SearchResults result={mockData} />);

        expect(wrapper).toMatchSnapshot();
    });
});
