import React from 'react';

const Input = ({ handleChange, placeholder, type }) => {
    return (
        <input placeholder = "Enter text"
        onChange = {handleChange}
        placeholder = { placeholder }
        type = { type }
        />
    )
}

export default Input