import React from "react";

const SearchResults = ({ result }) => {
    return (
        <div className="result">
        {(result.length===0)? null : <h3>Results</h3>}

        {result.map(res => {
            return (
            <li key={res.geonameid}>
                <span id="result_name">{res.name}</span> [{res.latitude},
                {res.longitude}]
            </li>
            );
        })}
        </div>
    );
};

export default SearchResults;
