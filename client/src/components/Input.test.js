import React from "react";
import { render } from "@testing-library/react";
import { shallow } from "enzyme";
import Input from "./Input";


describe("It runs all tests", () => {
    it("should have a placeholder", () => {
        const placeholderText = "keyword";
        const wrapper = shallow(<Input placeholder={placeholderText} />);
        expect(wrapper.prop("placeholder")).toEqual(placeholderText);
    });

    it("should render a correct type", () => {
        const type = "string";
        const wrapper = shallow(<Input type={type} />);
        expect(wrapper.prop("type")).toEqual(type);
    });

});
