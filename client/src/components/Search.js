import React from "react";
import SearchResults from "./SearchResults";
import Input from './Input';

const URL = "http://localhost:3000/api/locations";

class Search extends React.Component {
    state = {
        search: "",
        resultOutput: []
    };

    handleChange = e => {
        if (e.target.value.length >= 3) {
        this.setState({ search: e.target.value });
        this.getResults();
        }
    };

    getResults = () => {
        fetch(URL)
        .then(res => res.json())
        .then(data => {
            let regex = new RegExp(`^${this.state.search}`, "i");
            let apiResult = data.results.filter(res => res.name.match(regex));
            this.setState({
            resultOutput: apiResult
            });
        });
    };

    render() {
        return (
        <div className="Form">
            <h1>Geo Searcher</h1>
            <form>
            <h2>Begin your search!</h2>
            <Input
                handleChange={this.handleChange}
                placeholder='keyword'
                type='text'
            />
            </form>

            <div className="Results">
            <SearchResults result={this.state.resultOutput} />>
            </div>
        </div>
        );
    }
}

export default Search;
